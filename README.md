# McStrings

A barrel containing helper classes for working with strings in MonkeyC

## License

McStrings is available for use under the MIT license.
