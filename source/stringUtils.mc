import Toybox.Lang;
import Toybox.Graphics;


module Strings {

    function removeLeadingWhitespace(str as String, removeNewlines as Boolean) {
        var chars = str.toCharArray();
        var numLeadingWhitespace = 0;
        for (var i = 0; i < chars.size(); i++) {
            var char = chars[i];
            if (char == ' ' || char == '\t' || (removeNewlines && char == '\n')) {
                numLeadingWhitespace = i + 1;
            } else {
                break;
            }
        }
        return str.substring(numLeadingWhitespace, str.length());
    }

    function removeTrailingWhitespace(str as String, removeNewlines as Boolean) {
        var lastNonWhitespace = -1;
        var chars = str.toCharArray();
        for (var i = chars.size() - 1; i >= 0; i--) {
            var char = chars[i];
            if (!(char == ' ' || char == '\t' || (removeNewlines && char == '\n'))) {
                lastNonWhitespace = i;
                break;
            }
        }
        return str.substring(0, lastNonWhitespace + 1);
    }

    function trim(str as String, removeNewlines as Boolean) {
        return removeTrailingWhitespace(removeLeadingWhitespace(str, removeNewlines), removeNewlines);
    }

    function isAlphanumeric(string as String) as Boolean {
        // Test if string is alphanumeric
        var chars = string.toCharArray();
        for (var i = 0; i < chars.size(); i++) {
            var char = chars[i];
            if (!((char >= 'a' && char <= 'z') || (char >= 'A' && char <= 'Z') || 
                (char >= '0' && char <= '9'))) {
                return false;
            }
        }
        return true;
    }

    function replaceAll(searchFor as String, replaceWith as String, str as String) as String {
        // Replace all instances of searchFor with replaceWith in str
        // Will not converge if replaceWith contains searchFor
        while (true) {
            var occurrence = str.find(searchFor);
            if (occurrence != null) {
                str = 
                    str.substring(0, occurrence) + 
                    replaceWith + 
                    str.substring(occurrence + 1, str.length());
            } else {
                break;
            }
        }
        return str;
    }

    function join(lines as Array<String>, token as String) as String {
        /*
        Join an array of strings with a token
        */
        var result = "";
        for (var i = 0; i < lines.size(); i++) {
            if (i > 0) {
                result += token;
            }
            result += lines[i];
        }
        return result;
    }

    function wrap(string as String, width as Number, font as Graphics.FontReference, dc as Graphics.Dc) as Array<String> {
        /*
        Wrap a string into an array of line strings
        */
        var result = [];
        while (string.length() > 0) {
            var split = TextWrapper.getLineBreak(string, width, font, dc);
            var line = string.substring(0, split != null ? split : string.length());
            result.add(removeTrailingWhitespace(line, true));
            if (split == null) {
                break;
            } else {
                var endsOnNewline = string.substring(split, split + 1).equals("\n");
                string = string.substring(endsOnNewline ? split + 1 : split, string.length());
            }
        }
        return result;
    }

    function wrapLine(string as String, width as Number, font as Graphics.FontReference, dc as Graphics.Dc) as String {
        /*
        Wrap a string into a single line
        */
        var split = TextWrapper.getLineBreak(string, width, font, dc);
        var line = string.substring(0, split != null ? split : string.length());
        return removeTrailingWhitespace(line, true);
    }
}
