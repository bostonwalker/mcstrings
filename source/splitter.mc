import Toybox.Lang;
import Toybox.WatchUi;
import Toybox.System;
import Toybox.Test;

import Iterators;


module Strings {
        
    class Splitter extends Iterators.BaseIterator {

        // Non-state variables
        private var _inputStr as String or ResourceId;
        private var _token as String;

        // State variables
        private var _str as String?;
        private var _breaks as Array<Number?>;

        function initialize(inputStr as String or ResourceId, token as String) {
            /*
            An iterator to split a string on a token

            :param str: String or Resource ID
            :param token: Token to split on
            */
            Iterators.BaseIterator.initialize();
            
            _inputStr = inputStr;
            _token = token;

            _str = null;
            _breaks = [];
        }

        function peek() as Object? {
            if (_str == null) {
                _str = _loadString();
            }
            var start = seek > 0 ? _breaks[seek - 1] : 0;
            if (start == null) {
                return null;
            }
            if (seek == _breaks.size()) {
                // Ran out of memoized split points, find next
                var strRemaining = _str.substring(start, _str.length());
                var split = strRemaining.find(_token);
                if (split != null) {
                    _breaks.add(start + split + _token.length());
                } else {
                    _breaks.add(null);
                }
            }
            var end = _breaks[seek];
            if (end != null) {
                return _str.substring(start, end - _token.length());
            } else {
                return _str.substring(start, _str.length());
            }
        }

        function setSeek(seek as Number) {
            if (seek == 0) {
                // Reset
                self.seek = 0;
            } else {
                while (seek >= _breaks.size() && next() != null) {}
                if (seek >= 0 && seek < _breaks.size()) {
                    self.seek = seek;
                } else {
                    throw new Lang.InvalidValueException(
                        "Item " + seek.toString() + " of iterator does not exist");
                }
            }
        }

        function size() as Number {
            exploit();
            return _breaks.size();
        }

        function toString() as String {
            return "Splitter{" +
                "inputStr=" + _inputStr.toString() + "," +
                "token=" + _token.toString() + "}";
        }

        function unload() as Void {
            // Unload string resource
            _str = null;
        }

        private function _loadString() as String {
            if (_inputStr instanceof String) {
                return _inputStr;
            } else if (_inputStr instanceof ResourceId) {  // ResourceId maps to Number at runtime
                // Load from resources
                return WatchUi.loadResource(_inputStr as ResourceId) as String;
            } else {
                throw new Lang.InvalidValueException(
                    "Input string is of invalid type: " + _inputStr.toString());
            }
        }
    }


    (:test)
    function testSplitter(logger as Logger) as Boolean {

        var testInput;
        var testOutput;
        var obj;

        testInput = "A|B|C";
        testOutput = ["A", "B", "C"];
        System.println("Test input: \"" + testInput + "\"");
        obj = new Splitter(testInput, "|");
        if(!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        testInput = "A|B|C|";
        testOutput = ["A", "B", "C", ""];
        System.println("Test input: \"" + testInput + "\"");
        obj = new Splitter(testInput, "|");
        if(!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        testInput = "|A|B|C|";
        testOutput = ["", "A", "B", "C", ""];
        System.println("Test input: \"" + testInput + "\"");
        obj = new Splitter(testInput, "|");
        if(!obj.testAgainstOutput(testOutput)) {
            return false;
        }

        return true;
    }


    (:test)
    function testSplitterPeekAndHasNext(logger as Logger) as Boolean {

        var testInput = "Hello\nYou\nGuy";
        var obj = new Splitter(testInput, "\n");

        Test.assertEqual(obj.next(), "Hello");
        Test.assert(obj.hasNext());

        Test.assertEqual(obj.peek(), "You");
        Test.assertEqual(obj.next(), "You");

        Test.assertEqual(obj.next(), "Guy");
        Test.assert(!obj.hasNext());
        Test.assert(obj.peek() == null);
        Test.assert(obj.next() == null);

        return true;
    }


    (:test)
    function testSplitterGetAndSize(logger as Logger) as Boolean {

        var testInput = "Hello\nYou\nGuy";
        var obj;
        
        obj = new Splitter(testInput, "\n");
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(0), "Hello");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.peek(), "You");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.get(1), "You");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.peek(), "Guy");
        Test.assertEqual(obj.get(2), "Guy");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.size(), 3);

        obj = new Splitter(testInput, "\n");
        Test.assertEqual(obj.getSeek(), 0);
        Test.assertEqual(obj.get(2), "Guy");
        Test.assertEqual(obj.getSeek(), 3);
        Test.assert(obj.peek() == null);
        Test.assertEqual(obj.getSeek(), 3);
        Test.assertEqual(obj.get(1), "You");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.get(0), "Hello");
        Test.assertEqual(obj.getSeek(), 1);
        Test.assertEqual(obj.next(), "You");
        Test.assertEqual(obj.getSeek(), 2);
        Test.assertEqual(obj.size(), 3);

        obj = new Splitter(testInput, "\n");
        Test.assertEqual(obj.size(), 3);
        obj.setSeek(2);
        Test.assertEqual(obj.next(), "Guy");
        obj.setSeek(0);
        Test.assertEqual(obj.next(), "Hello");

        return true;
    }
}
